﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Reporting
{
    static class Program
    {
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            //string a = "";
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
